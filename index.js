//https://smarts.com.ar/
let productos = [];

let mysql = require('mysql');

const express = require("express");
const app = express();
app.use(express.json());

const path = require('path');

app.use(express.static('public'));
app.use('/public', express.static('public'));

app.get('/productos/', (req, res) =>{
    
    let connection = mysql.createConnection({
        host: 'localhost',
        user: 'Carlos',
        password: 'carlos',
        database: 'tecnoshop'
    })
    
    connection.connect((err) => {
        if (err) throw err;
        console.log('Conexión satisfactiora a la base de datos tecnoshop');
    })
    
    connection.query('SELECT * FROM productos', (err, rows) => {
        if (err) throw err;
    
        productos = rows;

        res.json(productos);
     
    })

    connection.end();

});

const port = 3001;
app.listen(port);
console.log(`Server is running on port ${port}`);
