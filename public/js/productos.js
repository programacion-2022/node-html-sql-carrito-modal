//LA API FETCH PROPORCIONA UNA INTERFAZ PARA RECUPERAR RECURSOS (INCLUSO, A TRAVÉS DE LA RED)
let traeProductos = () => {
    fetch('/productos/', { method: 'GET' })
        .then(response => response.json())
        .then(data => armaTemplate(data));

    function armaTemplate(productos) {
        console.log('Entré en armaTemplate');
        console.log(productos);
        let template = "";
        productos.forEach(producto => {
            let precioFormateado = new Intl.NumberFormat('es-AR').format(producto.precio);
            // Referencia para International Number Format: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat
            template +=
                `<article>
                    <div class="sumo" onclick="sumoItem(${producto.id},'${producto.descripcion}','${producto.imagen}',${producto.precio})">
                        <img src="agregar.png">
                    </div>
                    <h3 class="descripcion">${producto.descripcion}</h3>
                    <img src="${producto.imagen}" class="imagen">
                    <p>Precio $${precioFormateado}</p>
                </article>`
        });

        document.querySelector("#producto").innerHTML = template;
    }
}

traeProductos();
let carrito = [];
let contador = 0;
let productoEstaba;

function sumoItem(id, desc, imagen, precio) {
    console.log("Entro en sumo")
    productoEstaba = false;
    contador++;
    //ARMA EL OBJETO ITEM
    let item = {
        'id': id,
        'descripcion': desc,
        'imagen': imagen,
        'precio': precio,
        'cantidad': 0,
    }
    console.log("item: ", item);

    document.querySelector('#contador').innerHTML = contador;

    for (let i = 0; i < carrito.length; i++) {
        console.log("Dentro del for, carrito: ", carrito)
        console.log("Entré en el for: i: ", i, " - item.producto: ", item.id, " - carrito[i].producto: ", carrito[i].producto)
        if (item.id === carrito[i].id) {
            productoEstaba = true;
            carrito[i].cantidad++;
        }
    }
    if (!productoEstaba) {
        item.cantidad = 1;
        let elem = carrito.push(item);
    }
    console.log("Muestro el carrito: ", carrito);
}

function listado() {
    //MUESTRA LA VENTANA MODAL
    modal.style.opacity = 1;
    modal.style.visibility = "visible";
    // fetch('/productos/')
    //     .then(response => response.json())
    //     .then(data => armaModal(data));
    armaModal();
}
let totalCarrito;
//FUNCION DERIVADA DE LISTADO
function armaModal() {
    totalCarrito = 0;
    let html = "<table><thead><th>Imagen </th><th>ID</th><th>Descripción </th><th>Precio </th><th>Cantidad </th><th>Importe </th></thead><tbody>";
    for (let i = 0; i < carrito.length; i++) {
        console.log("Mostrando Carrito: ", carrito[i]);
        let producto = carrito[i];
        let producto_importe = producto.cantidad * producto.precio;
        totalCarrito = totalCarrito + producto_importe;
        console.log("Mostrando objeto producto: ", producto.descripcion, " - ", producto.precio, " - ", producto.cantidad);
        html += `<tr>
                     <td><img src='${producto.imagen}'></td>
                     <td>${producto.id}</td>
                     <td>${producto.descripcion}</td>
                     <td>${new Intl.NumberFormat('es-AR').format(producto.precio)}</td>
                     <td>
                         <div class="cuadradito" onclick="sumo(${producto.id})">+</div>
                         <div id="canti${producto.id}">${producto.cantidad}</div>
                         <div class="cuadradito" onclick="resto(${producto.id})">-</div>
                     </td>
                     <td><div id="importe${producto.id}">${new Intl.NumberFormat('es-AR').format(producto_importe)}</td>
                </tr>`;
    }
    html += `<td colspan="5" style="text-align: right; padding: 10px; font-weight: bold">Total compra: </td><td><div id="tc" style="font-weight: bold">${new Intl.NumberFormat('es-AR').format(totalCarrito)}</div></td>`
    html += "</tbody></table>";
    document.querySelector(".tabla").innerHTML = html;
}

function sumo(id) {
    console.log("estoy en sumo");
    //TOMO EL PRODUCTO DEL CARRITO SEGÚN EL ID Y LO ASIGNO A PRODUCTO
    let producto = carrito.find(element => element.id === id);
    console.log("Producto en sumo: ", producto);
    //DETERMINO LA POSICIÓN DONDE ENCONTRÓ EL ID
    let posicion = carrito.indexOf(producto);
    console.log(posicion);
    producto.cantidad++;
    //REEMPLAZO EL PRODUCTO EN EL CARRITO, EN LA POSICIÓN CORRESPONDIENTE
    carrito.splice(posicion, 1, producto);
    console.log("Carrito en la fila modificada: ", carrito[posicion]);
    console.log("Carrito completo dsps de sumar: ", carrito);    //DONDE VEMOS LA SUMA REALIZADA EN CANTIDAD
    //MUESTRO LA CANTIDAD NUEVA POR PANTALLA
    document.querySelector(`#canti${producto.id}`).innerHTML = producto.cantidad;
    let producto_importe = producto.cantidad * producto.precio;
    document.querySelector(`#importe${producto.id}`).innerHTML = new Intl.NumberFormat('es-AR').format(producto_importe);
    totalCarrito += producto.precio;
    console.log("totalCarrito: ", totalCarrito);
    document.querySelector('#tc').innerHTML = new Intl.NumberFormat('es-AR').format(totalCarrito);
    contador++;
    document.querySelector('#contador').innerHTML = contador;
}
function resto(id) {
    let producto = carrito.find(element => element.id === id);
    let posicion = carrito.indexOf(producto);
    producto.cantidad--;
    carrito.splice(posicion, 1, producto);
    document.querySelector(`#canti${producto.id}`).innerHTML = producto.cantidad;
    let producto_importe = producto.cantidad * producto.precio;
    document.querySelector(`#importe${producto.id}`).innerHTML = new Intl.NumberFormat('es-AR').format(producto_importe);
    totalCarrito -= producto.precio;
    console.log("totalCarrito: ", totalCarrito);
    document.querySelector('#tc').innerHTML = new Intl.NumberFormat('es-AR').format(totalCarrito);
    contador--;
    document.querySelector('#contador').innerHTML = contador;

    if (producto.cantidad === 0) {
        carrito.splice(posicion, 1);    //ELIMINA EL ELEMENTO DE CARRITO Y LUEGO ARMA EL MODAL DE NUEVO
        armaModal();
    }
}